package dev.hanburger.TileGame.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;

public class TutorialState extends State{

	private State gst;
	private BufferedImage backmap;

	public TutorialState(Game game, State gamestate) {
		super(game);
		
		gst = gamestate;
		backmap = new BufferedImage(160,144,6);
		
		Graphics2D bLoader = backmap.createGraphics();
		int height= 9;
		int width=10;
		int counter3 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				bLoader.drawImage(Assets.mapSprites.get(36),(int)((16*x)),
						(int)((16*y)),16,16,null);
				counter3++;
				
				
			}
		}
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		if(game.getKeyManager().x)
		{
			State.setState(gst);
		}
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
		
		Graphics2D g2 = (Graphics2D)g;
		
		g2.drawImage(backmap,0,0,160,144,null);
		
		g2.setFont(FontLoader.customFont.deriveFont(12f));
		//g2.setColor(new Color(82,127,57));
		g2.setColor(new Color(215,232,148));
		
		g2.drawString("Notice", 43, 20);
		
		g2.drawString("Controls",31 , 85);
		
		g2.setFont(FontLoader.customFont.deriveFont(8f));
		
		g2.drawString("This game was made ", 7, 40);
		g2.drawString("in 11 days for ", 23, 50);
		g2.drawString("GBJAM4", 55, 60);
		
		g2.drawString("A,D - Move",7,95);
		g2.drawString("W - Jump",7,105);
		g2.drawString("X - Enter Doors",7,115);
		
		g2.drawString("Press X to Continue",4,135);
		
		
		
		
	}

}
