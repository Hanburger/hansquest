package dev.hanburger.TileGame.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.audio.Music;
import dev.hanburger.TileGame.audio.SoundEffect;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;
import dev.hanburger.TileGame.gfx.Map;
import dev.hanburger.TileGame.gfx.MapLoader;

public class MenuState extends State {

	private int xLogo;
	private int yLogo;
	private int speed;
	private boolean stopped;
	private BufferedImage backmap;
	private State gamestate;
	private Map map;
	
	private Music music;
	
	
	private boolean musicStarted=false;
	private Music music2;

	public MenuState(Game game, State gamestate) {
		super(game);
		// TODO Auto-generated constructor stub
		xLogo = 7;
		yLogo=-20;
		speed=1;
		stopped=false;
		this.gamestate=gamestate;
		music = new Music("GBJAMDemo2");
		music2 = new Music("GBJAMDemo4");
		
		
		backmap = new BufferedImage(160,144,6);
		
		
		
		Graphics2D bLoader = backmap.createGraphics();
		int height= 9;
		int width=10;
		int counter3 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				bLoader.drawImage(Assets.mapSprites.get(4),(int)((16*x)-game.getGameCamera().getxOffset()),
						(int)((16*y)-game.getGameCamera().getyOffset()),16,16,null);
				counter3++;
				
				
			}
		}
		
		try {
			map = new MapLoader(game).loadMap("/maps/TITLE.txt","TITLE");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		if(!musicStarted)
		{
			musicStarted=true;
			music.start();
		}
		
		if(yLogo < 50)
			yLogo=yLogo+speed;
			
			if(yLogo >= 50 &&!stopped)
			{
				stopped=true;
				
				//State.setState(gst);
			}
			
		if(stopped && game.getKeyManager().x)
		{
			music.stop();
			music2.start();
			State.setState(gamestate);
			
			
		}
		
		if(stopped)
		{
			map.tick();
		}
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(backmap,0,0,null);
		if(stopped)
		{
			map.render(g);
		}
		
		g2.setColor(new Color(32,70,49));
		
		g2.setFont(FontLoader.customFont2.deriveFont(30f));
		
		//g2.setFont(FontLoader.customFont.deriveFont(13f));
		
		g2.drawString("Han's Quest", xLogo, yLogo);
		
		if(stopped)
		{
			g2.setFont(FontLoader.customFont2.deriveFont(14f));
			g2.setColor(new Color(174,196,64));
			g2.drawString("Developed by", 160/2-75, 120);
			g2.drawString("Music by", 160/2-55-3, 130);
			
			//g2.setFont(FontLoader.customFont.deriveFont(8f));
			g2.drawString("@Hanburgah", 160/2 +5, 120);
			g2.drawString("@ACMenes", 160/2 , 130);
			
			
			g2.setFont(FontLoader.customFont2.deriveFont(16f));
			g2.drawString("Press X to Start!", 24, 100);
			
		}
		
	}

}
