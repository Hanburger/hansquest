package dev.hanburger.TileGame.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.Timer;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.audio.SoundEffect;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;

public class StartState extends State implements ActionListener{
	
	
	private float speed;
	private float x,y;
	private State gst;
	private Timer timer;
	private boolean stopped=false;
	private BufferedImage backmap;
	private SoundEffect startup;
	
	
	public StartState(Game game,State gameState)
	{
		super(game);
		this.x=0;
		this.y=0;
		this.gst=gameState;
		speed=0.5f;
		timer = new Timer(2500, this);
		backmap = new BufferedImage(160,144,6);
		
		startup = new SoundEffect("GameboyStartupSound");
		
		Graphics2D bLoader = backmap.createGraphics();
		int height= 9;
		int width=10;
		int counter3 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				bLoader.drawImage(Assets.mapSprites.get(4),(int)((16*x)-game.getGameCamera().getxOffset()),
						(int)((16*y)-game.getGameCamera().getyOffset()),16,16,null);
				counter3++;
				
				
			}
		}
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		if(y < 74)
		y=y+speed;
		
		if(y >= 74 &&!stopped)
		{
			stopped=true;
			startup.play();
			timer.start();
			//State.setState(gst);
		}
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(backmap,0,0,null);
		g2.setFont(FontLoader.customFont2.deriveFont(32f));
		g2.setColor(new Color(32,70,49));
		
		g2.drawString("Nintendo", x+21, y);
	}

	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		timer.stop();
		State.setState(gst);
		
	}

}
