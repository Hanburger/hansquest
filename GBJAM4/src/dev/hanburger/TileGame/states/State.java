package dev.hanburger.TileGame.states;

import java.awt.Graphics;

import dev.hanburger.TileGame.Game;

public abstract class State {
	
	private static State currentState = null;
	
	public static void setState(State state)
	{
		currentState = state;
		System.out.println("dd");
	}
	
	public static State getState()
	{
		return currentState;
		
	}
	
	//ClASS GAME
	
	protected Game game;
	
	public State (Game game)
	{
		this.game = game;
	}
	
	public abstract void tick();
	
	public abstract void render(Graphics g);
	
	
	

}
