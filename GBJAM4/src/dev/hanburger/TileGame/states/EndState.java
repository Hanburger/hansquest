package dev.hanburger.TileGame.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;

public class EndState extends State {
	
	
	
	
	private BufferedImage backmap;
	
	private boolean won;
	
	private int time;

	public EndState(Game game,int collected,int max, int time)
	{
		super(game);
		if(collected == max)
		{
			won =true;
		}
		else
		{
			won = false;
		}
		
		this.time=time;
		
		backmap = new BufferedImage(160,144,6);
		
		Graphics2D bLoader = backmap.createGraphics();
		int height= 9;
		int width=10;
		int counter3 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				bLoader.drawImage(Assets.mapSprites.get(36),(int)((16*x)),
						(int)((16*y)),16,16,null);
				counter3++;
				
				
			}
		}
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		Graphics2D g2 = (Graphics2D)g;
		
		g2.drawImage(backmap,0,0,160,144,null);
		
		g2.setFont(FontLoader.customFont.deriveFont(8f));
		//g2.setColor(new Color(82,127,57));
		g2.setColor(new Color(215,232,148));
		
		
		if(won)
		{
			g2.drawString("You have won!", 27, 50);
			g2.drawString("Time: " + String.valueOf(time), 49, 70);
		}
		else
		{
			g2.drawString("You have lost!", 27, 50);
			g2.drawString("...Shame on you", 19, 110);
		}
		
		
			
			
	}

}
