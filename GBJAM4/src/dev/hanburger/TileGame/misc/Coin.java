package dev.hanburger.TileGame.misc;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;

public class Coin {
	
	private float x,y;
	private Rectangle2D.Float box;
	private Game game;
	private String MAPNAME;
	
	public String getMAPNAME() {
		return MAPNAME;
	}

	public void setMAPNAME(String mAPNAME) {
		MAPNAME = mAPNAME;
	}

	public Coin(float x, float y,Game game, String name)
	{
		this.x=x;
		this.y=y;
		this.game=game;
		this.MAPNAME= name;
		box = new Rectangle2D.Float(x,y,16,16);
		
	}
	
	public void render(Graphics g)
	{
		g.drawImage(Assets.sprites.get(166),(int)(x-game.getGameCamera().getxOffset()),
				(int)(y-game.getGameCamera().getyOffset()),16,16,null);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public Rectangle2D.Float getBox() {
		return box;
	}

}
