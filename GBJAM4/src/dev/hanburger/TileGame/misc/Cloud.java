package dev.hanburger.TileGame.misc;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Cloud {
	
	private float x;
	private float y;
	private BufferedImage cloud;
	private int speed;
	private int size;

	public Cloud(float x, float y, BufferedImage cloud,int speed,int size)
	{
		this.x=x;
		this.y=y;
		this.cloud=cloud;
		this.speed=speed;
		this.size=size;
		
	}
	
	
	public void tick()
	{
		x=x-speed;
		
		
		
		if(x< -size*6)
		{
			//System.out.println("dd");
			x=160;
		}
	}
	
	public void render(Graphics g)
	{
		g.drawImage(cloud,(int) x,(int) y, null);
		
	}


}
