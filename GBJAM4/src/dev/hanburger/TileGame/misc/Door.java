package dev.hanburger.TileGame.misc;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;

public class Door {
	
	private float x,y;
	private Rectangle2D.Float box;
	private Game game;
	private String MAPNAME;
	
	private int warpToMap;
	private int warpToDoor;
	
	
	
	public Door(float x, float y, Game game,String MAPNAME, int warpToMap,int warpToDoor)
	{
		this.x=x;
		this.y=y;
		box = new Rectangle2D.Float(x, y, 16, 16);
		this.game=game;
		this.MAPNAME=MAPNAME;
	//	this.doorNumber
		
		this.warpToMap=warpToMap;
		this.warpToDoor=warpToDoor;
	}
	
	
	public String getMAPNAME()
	{
		return MAPNAME;
	}
	
	
	public int getWarpToMap() {
		return warpToMap;
	}





	public int getWarpToDoor() {
		return warpToDoor;
	}





	public void render(Graphics g)
	{
		g.drawImage(Assets.sprites.get(166),(int)(x-game.getGameCamera().getxOffset()),
				(int)(y-game.getGameCamera().getyOffset()),16,16,null);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public Rectangle2D.Float getBox() {
		return box;
	}

}



