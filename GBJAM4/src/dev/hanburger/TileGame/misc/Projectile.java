package dev.hanburger.TileGame.misc;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;

public class Projectile {
	
	
	
	private float y;
	private float x;
	private String direction;
	private Rectangle2D.Float box;
	private Game game;
	

	public Projectile(float x, float y,String direction,Game game)
	{
		this.x=x;
		this.y=y;
		this.direction=direction;
		this.game=game;
		
		box = new Rectangle2D.Float(x,y,16,16);
		
		
	}
	
	public void tick()
	{
		if(direction.equals("LEFT"))
		{
			x-=3;
		}
		else
		{
			x+=3;
		}
		box.setRect(x, y-16, 16, 16);
		
		
	}
	
	public float getX()
	{
		return x;
	}
	
	public Rectangle2D getBox()
	{
		return box;
	}
	
	
	
	
	public void render(Graphics g)
	{
		g.drawImage(Assets.sprites.get(173),(int)x-(int)game.getGameCamera().getxOffset(),
				(int)y-(int)game.getGameCamera().getyOffset()-15,16,16,null);
		
		
	}

}
