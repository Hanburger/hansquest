package dev.hanburger.TileGame;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import dev.hanburger.TileGame.CollisionDetection.CollisionDetection;
import dev.hanburger.TileGame.display.Display;
import dev.hanburger.TileGame.entities.creatures.Player;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.FontLoader;
import dev.hanburger.TileGame.gfx.GameCamera;
import dev.hanburger.TileGame.gfx.MapManager;
import dev.hanburger.TileGame.input.KeyManager;
import dev.hanburger.TileGame.states.GameState;
import dev.hanburger.TileGame.states.MenuState;
import dev.hanburger.TileGame.states.StartState;
import dev.hanburger.TileGame.states.State;
import dev.hanburger.TileGame.states.TutorialState;

public class Game implements Runnable {
	
	private Display display;
	
	
	
	public int width,height;
	public String title;
	
	private boolean running=false;
	private Thread thread;
	
	private BufferStrategy bs;
	private Graphics g;
	
	
	private State gameState;
	public State menuState;
	
	private KeyManager keyManager;
	
	private MapManager mapManager;
	
	private GameCamera camera;
	
	private CollisionDetection colDet;
	
	private StartState startState;
	
	private Player player;
	private static int tilewidth;
	
	public Game(String title,int width,int height, int tilewidth)
	{
		this.width=width;
		this.height=height;
		this.title=title;
		this.tilewidth=tilewidth;
		keyManager= new KeyManager();
		
		System.setProperty("sun.java2d.opengl", "True");
		
		
	}
	int x=0;



	private TutorialState tutState;



	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		init();
		
		int fps = 60;
		double timePerTick = 1000000000 /fps;
		double delta =0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;
		
		
		while(running)
		{
			now = System.nanoTime();
			delta+=(now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			
			
			
			if(delta >= 1)
			{
				tick();
				render();
				ticks++;
				delta --;
			}
			
			if(timer >= 1000000000)
			{
				System.out.println("Times and Frames: " + ticks);
				ticks=0;
				timer=0;
			}
		}
		
		stop();
	}
	
	private void init() {
		// TODO Auto-generated method stub
		display = new Display(title,width,height);
		display.getFrame().addKeyListener(keyManager);
		Assets.init();
		FontLoader.loadFont();
		camera = new GameCamera(this,0,0);
		colDet = new CollisionDetection(this);
		player = new Player(this,64,64+16*6);
		mapManager= new MapManager(this,player);
		gameState = new GameState(this,player);
		tutState = new TutorialState(this,gameState);
		menuState = new MenuState(this,tutState);
		startState = new StartState(this,menuState);
		
		State.setState(startState);
	}
	
	
	public CollisionDetection getColDet() {
		return colDet;
	}

	public MapManager getMapManager() {
		return mapManager;
	}

	private void tick()
	{
		keyManager.tick();
		if(State.getState()!=null)
		{
			State.getState().tick();
		}
	}
	
	private void render()
	{
		bs = display.getCanvas().getBufferStrategy();
		if(bs == null)
		{
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		
		g = bs.getDrawGraphics();
		
		g.clearRect(0, 0, width, height);
		
		
		//g.drawImage(Assets.player, x, 10,32,32, null);
		if(State.getState()!=null)
		{
			State.getState().render(g);
		}
		
		
		bs.show();
		g.dispose();
	}


	public synchronized void start()
	{
		if(running)
		{
			return;
		}
		
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public synchronized void stop()
	{
		if(!running)
		{
			return;
		}
		running =false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public KeyManager getKeyManager()
	{
		return keyManager;
	}
	
	public GameCamera getGameCamera()
	{
		return camera;
	}
	

}
