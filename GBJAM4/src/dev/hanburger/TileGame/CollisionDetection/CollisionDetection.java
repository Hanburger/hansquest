package dev.hanburger.TileGame.CollisionDetection;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.Entity;
import dev.hanburger.TileGame.entities.creatures.Npc;
import dev.hanburger.TileGame.misc.Coin;
import dev.hanburger.TileGame.misc.Door;

public class CollisionDetection {
	
	private ArrayList<Entity> entities;
	//private ArrayList<Rectangle2D> blocks;
	private TreeMap<String,ArrayList<Rectangle2D>> blockMaps;
	private ArrayList<Integer> collisionIDS;
	private ArrayList<Coin> coins;
	private Game game;
	
	
	public CollisionDetection(Game game)
	{
		entities = new ArrayList<>();
		//blocks = new ArrayList<>();
		coins = new ArrayList<>();
		blockMaps= new TreeMap<String,ArrayList<Rectangle2D>>();
		
		collisionIDS= new ArrayList<>();
		
		this.game=game;
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		collisionIDS.add(6);
		collisionIDS.add(7);
		collisionIDS.add(8);
		collisionIDS.add(1);
		collisionIDS.add(2);
		collisionIDS.add(3);
		collisionIDS.add(9);
		
		collisionIDS.add(10);
		collisionIDS.add(11);
		
		
		
		
	}
	
	public void addMap(String MAP)
	{
		blockMaps.put(MAP, new ArrayList<Rectangle2D>());
	}
	public ArrayList<Rectangle2D> getMap(String MAP)
	{
		return blockMaps.get(MAP);
	}
	
	public boolean checkCollision(float x, float y,Entity b,String MAP)
	{
		for(Rectangle2D e : blockMaps.get(MAP))
		{
			if(e.intersects(x, y, 16, 16))
			{
				return true;
				
			}
			
		}
		
		for(Entity e : entities)
		{
			if(e.getBox().intersects(x, y, 16, 16) && !e.equals(b))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public Npc returnNPCCollision(float x,float y, Entity b)
	{
		for(Entity e : entities)
		{
			if(e.getBox().intersects(x, y, 16, 16) && !e.equals(b))
			{
				if(e instanceof Npc)
				return (Npc)e;
			}
		}
		
		return null;
	}
	
	public Coin checkForCoin(float x, float y,String MAPNAME)
	{
		Iterator<Coin> it = coins.iterator();
		
		while(it.hasNext())
		{
			Coin currentCoin = it.next();
			
			if(currentCoin.getBox().intersects(x, y, 16, 16) && currentCoin.getMAPNAME()== MAPNAME)
			{
				Coin returningcoin = new Coin(currentCoin.getX(),currentCoin.getY(),game,currentCoin.getMAPNAME()); 
				it.remove();
				return returningcoin;
			}
		}
		
		return null;
	}
	
	

	public ArrayList<Integer> getCollisionIDS() {
		return collisionIDS;
	}

	public void setCollisionIDS(ArrayList<Integer> collisionIDS) {
		this.collisionIDS = collisionIDS;
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

//	public ArrayList<Rectangle2D> getBlocks() {
//		return blocks;
//	}
	
	public ArrayList<Coin> getCoins()
	{
		return coins;
	}
	
	
	
	
	

}
