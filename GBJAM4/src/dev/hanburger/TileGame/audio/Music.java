package dev.hanburger.TileGame.audio;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Music implements Runnable {

		
	URL url;
	AudioInputStream ais;
	Clip clip;
	String fn;
	private Thread thread;
	
	
	public Music (String fn)
	{
		thread = new Thread(this);
		this.fn=fn;
		//thread.start();
	}
	
	public void start()
	{
		thread.start();
	}
	
	public void stop()
	{
		clip.stop();
	}
	
	
	
	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		try
		{
			//url = SoundEffect.class.getResource("/music/"+ fn + ".wav");
			InputStream audioSrc = SoundEffect.class.getResourceAsStream("/sound/"+ fn + ".wav");
			InputStream bufferedIn = new BufferedInputStream(audioSrc);
			
			//ais = AudioSystem.getAudioInputStream(SoundEffect.class.getResourceAsStream("/sound/"+ fn + ".wav"));
			ais = AudioSystem.getAudioInputStream(bufferedIn);
			clip = AudioSystem.getClip();
			clip.open(ais);
			//clip.start(); 
    
		} 
		catch(Exception e){e.printStackTrace();}
		
		clip.loop(clip.LOOP_CONTINUOUSLY);
		
		while(clip.isRunning())
		{
			
		}
		
	}

	
	
	
	

}
