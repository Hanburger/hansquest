package dev.hanburger.TileGame.audio;

	
	
	import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
	
	public class SoundEffect implements Runnable
	{
		URL url;
		AudioInputStream ais;
		Clip clip;
		String fn;
		private Thread thread;
	 
		public SoundEffect(String fn)
		{
			this.fn=fn;
			thread = new Thread(this);
			thread.start();
			
		}
	 
		public void play()
		{
			if(!clip.isRunning())
			{
				clip.start();
				thread = new Thread(this);
				thread.start();
			}
		 		//reInit();
				
		 
		 		// System.out.println(clip.isRunning());
		}
		public void reInit()
		{
			try {
				ais = AudioSystem.getAudioInputStream(SoundEffect.class.getResource("/sound/"+ fn + ".wav"));
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				clip = AudioSystem.getClip();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				clip.open(ais);
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try
			{
				//url = SoundEffect.class.getResource("/music/"+ fn + ".wav");
				InputStream audioSrc = SoundEffect.class.getResourceAsStream("/sound/"+ fn + ".wav");
				InputStream bufferedIn = new BufferedInputStream(audioSrc);
				
				//ais = AudioSystem.getAudioInputStream(SoundEffect.class.getResourceAsStream("/sound/"+ fn + ".wav"));
				ais = AudioSystem.getAudioInputStream(bufferedIn);
				clip = AudioSystem.getClip();
				clip.open(ais);
				//clip.start(); 
	    
			} 
			catch(Exception e){e.printStackTrace();}
		}
	}


