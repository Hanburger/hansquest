package dev.hanburger.TileGame.gfx;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Timer;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.audio.SoundEffect;
import dev.hanburger.TileGame.entities.creatures.Player;
import dev.hanburger.TileGame.entities.creatures.Turret;
import dev.hanburger.TileGame.misc.Door;
import dev.hanburger.TileGame.misc.Projectile;

public class MapManager implements ActionListener {
	
	private ArrayList<Map> maps;
	
	private int currentMap;
	
	private Game game;
	
	private ArrayList<Door> doors;

	private Player player;

	private BufferedImage backmap;
	
	private boolean switcher=false;
	
	private Timer timer;
	
	private boolean damaged=false;
	private boolean justbeendamaged=false;

	private int damageCounter=0;
	
	
	public MapManager(Game game,Player player)
	{
		maps = new ArrayList<>();
		this.player=player;
		this.game=game;
		currentMap=0;
		doors = new ArrayList<>();
		timer = new Timer(400,this);
		init();
	}
	
	
	private void init() {
		// TODO Auto-generated method stub
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL01.txt","LEVEL01"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doorsC= maps.get(0).getDoorCoords();
		
		ArrayList<Door> doorsTEST6 = new ArrayList<>();
		doorsTEST6.add(new Door((float)doorsC.get(0).getX(),(float)doorsC.get(0).getY(),
				game, "LEVEL01", 1, 0) );
		maps.get(0).setDoors(doorsTEST6);
		
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL02.txt","LEVEL02"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doorsC2= maps.get(1).getDoorCoords();
		
		ArrayList<Door> doorsTEST7 = new ArrayList<>();
		doorsTEST7.add(new Door((float)doorsC2.get(0).getX(),(float)doorsC2.get(0).getY(),
				game, "LEVEL02", 0, 0) );
		doorsTEST7.add(new Door((float)doorsC2.get(1).getX(),(float)doorsC2.get(1).getY(),
				game,"LEVEL02",2,0));
		maps.get(1).setDoors(doorsTEST7);
		
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL03.txt","LEVEL03"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doorsC3= maps.get(2).getDoorCoords();
		
		ArrayList<Door> doorsTEST8 = new ArrayList<>();
		doorsTEST8.add(new Door((float)doorsC3.get(0).getX(),(float)doorsC3.get(0).getY(),
				game, "LEVEL03", 1, 1) );
		doorsTEST8.add(new Door((float)doorsC3.get(1).getX(),(float)doorsC3.get(1).getY(),
				game,"LEVEL03",3,1));
		maps.get(2).setDoors(doorsTEST8);
		
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL04.txt","LEVEL04"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors04= maps.get(3).getDoorCoords();
		
		ArrayList<Door> doorsLvl04 = new ArrayList<>();
		doorsLvl04.add(new Door((float)doors04.get(0).getX(),(float)doors04.get(0).getY(),
				game, "LEVEL04", 4, 0) );
		doorsLvl04.add(new Door((float)doors04.get(1).getX(),(float)doors04.get(1).getY(),
				game,"LEVEL04",2,1));
		
		doorsLvl04.add(new Door((float) doors04.get(2).getX(),(float)doors04.get(2).getY(),
				game,"LEVEL04",7,0));
		doorsLvl04.add(new Door((float) doors04.get(3).getX(),(float)doors04.get(3).getY(),
				game,"LEVEL04",5,2));
		
		doorsLvl04.add(new Door((float) doors04.get(4).getX(),(float)doors04.get(4).getY(),
				game,"LEVEL04",8,0));
		
		
		maps.get(3).setDoors(doorsLvl04);
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL05.txt","LEVEL05"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors05= maps.get(4).getDoorCoords();
		
		ArrayList<Door> doorsLvl05 = new ArrayList<>();
		doorsLvl05.add(new Door((float)doors05.get(0).getX(),(float)doors05.get(0).getY(),
				game, "LEVEL05", 3, 0) );
		doorsLvl05.add(new Door((float)doors05.get(1).getX(),(float)doors05.get(1).getY(),
				game,"LEVEL05",5,0));
		
		
		maps.get(4).setDoors(doorsLvl05);
		
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL06.txt","LEVEL06"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors06= maps.get(5).getDoorCoords();
		
		ArrayList<Door> doorsLvl06 = new ArrayList<>();
		doorsLvl06.add(new Door((float)doors06.get(0).getX(),(float)doors06.get(0).getY(),
				game, "LEVEL06", 4, 1) );
		doorsLvl06.add(new Door((float)doors06.get(1).getX(),(float)doors06.get(1).getY(),
				game,"LEVEL06",6,0));
		
		doorsLvl06.add(new Door((float) doors06.get(2).getX(),(float)doors06.get(2).getY(),
				game,"LEVEL06",3,3));
	
		
		maps.get(5).setDoors(doorsLvl06);
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL07.txt","LEVEL07"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors07= maps.get(6).getDoorCoords();
		
		ArrayList<Door> doorsLvl07 = new ArrayList<>();
		doorsLvl07.add(new Door((float)doors07.get(0).getX(),(float)doors07.get(0).getY(),
				game, "LEVEL04", 5, 1) );
		
		
		
		maps.get(6).setDoors(doorsLvl07);
		
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL08.txt","LEVEL08"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors08= maps.get(7).getDoorCoords();
		
		ArrayList<Door> doorsLvl08 = new ArrayList<>();
		doorsLvl08.add(new Door((float)doors08.get(0).getX(),(float)doors08.get(0).getY(),
				game, "LEVEL08", 3, 2) );
		
		maps.get(7).setDoors(doorsLvl08);
		
		try {
			maps.add(new MapLoader(game).loadMap("/maps/LEVEL10.txt","LEVEL10"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//maps.get(0).setMAPNAME("TEST4");
		ArrayList<Point2D> doors09= maps.get(8).getDoorCoords();
		
		ArrayList<Door> doorsLvl09 = new ArrayList<>();
		doorsLvl09.add(new Door((float)doors09.get(0).getX(),(float)doors09.get(0).getY(),
				game, "LEVEL09", 3, 4) );
		
		
		
		maps.get(8).setDoors(doorsLvl09);
		
		
		
		
		
		
		
		
		
		
		
		
		backmap = new BufferedImage(160,144,6);
		
		
		
		Graphics2D bLoader = backmap.createGraphics();
		int height= 9;
		int width=10;
		int counter3 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				bLoader.drawImage(Assets.mapSprites.get(36),(int)((16*x)-game.getGameCamera().getxOffset()),
						(int)((16*y)-game.getGameCamera().getyOffset()),16,16,null);
				counter3++;
				
				
			}
		}
		
		
	}


	public void tick()
	{
		damageCounter=0;
		maps.get(currentMap).tick();
		
		for(Turret t : maps.get(currentMap).getTurrets())
		{
			for(Projectile e : t.getMissiles())
			{
				//System.out.println(e.getBox().getX()+", " + e.getBox().getY() + ", " + player.getX()+ ", " +player.getY());
				if(e.getBox().intersects(player.getBox()))
				{
					if(!damaged)
					player.damage();
					
					damaged=true;
					damageCounter++;
				}
			}
		}
		
		if(damageCounter==0)
		{
			damaged=false;
		}
		//maps.get(currentMap).getTurrets();

		
		if(game.getKeyManager().x)
		{
			for(int i = 0 ; i < maps.get(currentMap).getDoors().size(); i++)
			{
				if(maps.get(currentMap).getDoors().get(i).getBox().intersects(player.getBox()))
				{
					int door = maps.get(currentMap).getDoors().get(i).getWarpToDoor();
					currentMap= maps.get(currentMap).getDoors().get(i).getWarpToMap();
					//player.setBox(maps.get(currentMap).getDoors().get(i).getBox());
					
					player.setCoords( maps.get(currentMap).getDoors().get(door).getX(),
							 maps.get(currentMap).getDoors().get(door).getY());
					player.setCurrentMap(maps.get(currentMap).getMAPNAME());
					player.setBorders(maps.get(currentMap).getWidth()*16, 0);
					switcher=true;
					timer.start();
				}
			}
		}
		
		if(player.getY() > maps.get(currentMap).getHeight()*16)
		{
			player.setCoords(maps.get(currentMap).getDoors().get(0).getX(),
					maps.get(currentMap).getDoors().get(0).getY());
		}
	}
	
	public void render(Graphics g)
	{
		maps.get(currentMap).render(g);
		if(switcher)
		{
			g.drawImage(backmap,0,0,null);
			
		}
	}
	
	public Map returnCurrentMap()
	{
		return maps.get(currentMap);
	}
	
	public String returnCurrentMapName()
	{
		return maps.get(currentMap).getMAPNAME();
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		timer.stop();
		switcher=false;
	}

}
