package dev.hanburger.TileGame.gfx;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FontLoader {
	
	public static Font customFont;
	public static Font customFont2;
	
	public static void loadFont()
	{
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT,FontLoader.class.getResourceAsStream("/font/PressStart2P.ttf")).deriveFont(20f); 
			
			GraphicsEnvironment ge = 
		         GraphicsEnvironment.getLocalGraphicsEnvironment();
		    ge.registerFont(customFont); 
			//ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,FontLoader.class.getResourceAsStream("/font/PressStart2P.ttf")));
		} catch (IOException|FontFormatException e) {
		     //Handle exception
			System.out.println("OOPS,font not found!");
		}
		
		try {
			customFont2 = Font.createFont(Font.TRUETYPE_FONT,FontLoader.class.getResourceAsStream("/font/gbb.ttf")).deriveFont(20f); 
			
			GraphicsEnvironment ge = 
		         GraphicsEnvironment.getLocalGraphicsEnvironment();
		    ge.registerFont(customFont); 
			//ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,FontLoader.class.getResourceAsStream("/font/PressStart2P.ttf")));
		} catch (IOException|FontFormatException e) {
		     //Handle exception
			System.out.println("OOPS,font not found!");
		}

	}

}
