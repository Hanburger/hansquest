package dev.hanburger.TileGame.gfx;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.creatures.Npc;
import dev.hanburger.TileGame.entities.creatures.Turret;
import dev.hanburger.TileGame.misc.Cloud;
import dev.hanburger.TileGame.misc.Coin;
import dev.hanburger.TileGame.misc.Door;

public class Map {
	
	private int width,height;
	
	private ArrayList<BufferedImage> imageData;
	private ArrayList<Integer> data;
	private ArrayList<Animation> animationSprites;
	private ArrayList<Npc> npcs;
	private int animationCounter=0;
	private int cloudCounter =0 ;
	private String MAPNAME="";
	
	
	
	private ArrayList<Turret> turrets;
	
	
	
	private Game game;

	private BufferedImage map;

	private BufferedImage backmap;

	private ArrayList<Cloud> bigClouds;
	
	private ArrayList<Point2D> doorCoords;
	private ArrayList<Door> doors;
	
	public Map(Game game, int width,int height, ArrayList<Integer> data,String MapName)
	{
		this.width=width;
		this.height=height;
		this.game=game;
		this.MAPNAME=MapName;
		if(game==null)
		{
			System.out.println("I mean wut");
		}
		imageData = new ArrayList<>();
		this.data=data;
		doorCoords=new ArrayList<>();
		doors=new ArrayList<>();
		turrets= new ArrayList<>();
		npcs= new ArrayList<>();
		
		init();
		
		
		
	}
	
	public String getMAPNAME() {
		return MAPNAME;
	}

	public void setMAPNAME(String mAPNAME) {
		MAPNAME = mAPNAME;
	}

	
	public void setDoors(ArrayList<Door> doors)
	{
		this.doors=doors;
	}
	
	public ArrayList<Door> getDoors()
	{
		return doors;
	}
	public ArrayList<Point2D> getDoorCoords()
	{
		return doorCoords;
	}

	private void init() {
		// TODO Auto-generated method stub
		game.getColDet().addMap(MAPNAME);
		
		animationSprites = new ArrayList<>();
		
		for(Integer e : data)
		{
			if(Assets.returnAnimationSprites(e) == null && e!=0)
			{
				
				imageData.add(Assets.mapSprites.get(e));
			
			}
			else
			{
				imageData.add(Assets.mapSprites.get(97));
				
			}
			
			
		}
		int counter =0;
		for(int y = 0 ; y < height ; y++)
		{
			for(int x = 0; x < width; x++)
			{
				if(Assets.animationIDS.contains(data.get(counter)))
				{
					Animation animation  =  new Animation(Assets.returnAnimationSprites(data.get(counter)),
							16*x - game.getGameCamera().getxOffset(),16*y - game.getGameCamera().getyOffset(),game);
					
					if(data.get(counter) == 200)
					{
						animation.setsize(5*16, 5*16);
					}
					animationSprites.add(animation);
				}
				
				if(game.getColDet().getCollisionIDS().contains(data.get(counter)))
				{
					game.getColDet().getMap(MAPNAME).add(new Rectangle2D.Float(16*x, 16*y, 16, 16));
				}
				
				if(data.get(counter)==168)
				{
					Coin coin = new Coin(16*x,16*y,game,MAPNAME);
				
					game.getColDet().getCoins().add(coin);
				}
				
				if(data.get(counter)== 324)
				{
					doorCoords.add(new Point2D.Float(16*x,16*y));
				}
				
				if(data.get(counter) ==173)
				{
					turrets.add(new Turret(game,16*x,16*y,100, width));
				}
				
				if(data.get(counter) == 289)
				{
					npcs.add(new Npc(game, 16*x, 16*y, MAPNAME));
				}
				
				counter++;
			}
		}
		
		map = new BufferedImage(width*16,height*16,6);
//		backmap = new BufferedImage(width*16,height*16,6);
//		
//		Graphics2D bLoader = backmap.createGraphics();
//		
//		int counter3 =0;
//		for(int y = 0; y < height; y++)
//		{
//			for(int x = 0 ; x < width; x++)
//			{
//				bLoader.drawImage(Assets.mapSprites.get(4),(int)((16*x)-game.getGameCamera().getxOffset()),
//						(int)((16*y)-game.getGameCamera().getyOffset()),16,16,null);
//				counter3++;
//				
//				
//			}
//		}
		
		Graphics2D loader = map.createGraphics();
		
		int counter2 =0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0 ; x < width; x++)
			{
				loader.drawImage(imageData.get(counter2),(int)((16*x)-game.getGameCamera().getxOffset()),
						(int)((16*y)-game.getGameCamera().getyOffset()),16,16,null);
				
				
				counter2++;
				
				
			}
		}
		
		
		bigClouds = new ArrayList<>();
		bigClouds.add(new Cloud(20,10,Assets.bigCloud,4,6));
		bigClouds.add(new Cloud(80,10,Assets.bigCloud,4,6));
		bigClouds.add(new Cloud(140,10,Assets.bigCloud,4,6));
		bigClouds.add(new Cloud(50,22,Assets.smallCloud,3,4));
		bigClouds.add(new Cloud(95,22,Assets.smallCloud,3,4));
		bigClouds.add(new Cloud(144,22,Assets.smallCloud,3,4));
		
		
		
	}
	
	
	public ArrayList<Turret> getTurrets()
	{
		return turrets;
	}
	
	public void tick()
	{
		//System.out.println(turrets.size());
		
		if(animationCounter < 35)
		{
			animationCounter++;
		}
		else
		{
			animationCounter=0;
			for(Animation e : animationSprites)
			{
				
				e.update();
			}
		}
		
//		for(Animation e : animationSprites)
//		{
//			e.setCameraOffset(game.getGameCamera().getxOffset(),
//					game.getGameCamera().getyOffset());
//		}
		
		if(cloudCounter < 40)
		{
			cloudCounter++;
		}
		else
		{
			cloudCounter=0;
			for(Cloud c : bigClouds)
			{
				c.tick();
			}
		}

		for(Turret e : turrets)
		{
			e.tick();
		}
		
		for(Npc n : npcs)
		{
			n.tick();
		}
			
		
	}
	
	public void render(Graphics g)
	{
		
		//System.out.println(width + ", " + height);
//		int counter =0;
//			for(int y = 0; y < height; y++)
//			{
//				for(int x = 0 ; x < width; x++)
//				{
//					g.drawImage(imageData.get(counter),(int)((32*x)-game.getGameCamera().getxOffset()),
//							(int)((32*y)-game.getGameCamera().getyOffset()),32,32,null);
//					counter++;
//					
//					
//				}
//			}
		
		//g.drawImage(backmap,(int)( 0-game.getGameCamera().getxOffset()),(int)( 0-game.getGameCamera().getyOffset()), null);
		g.drawImage(Assets.background, (int)(0), 
				(int)(0), null);
		for(Cloud c : bigClouds)
		{
			c.render(g);
		}
		
		g.drawImage(map,(int)( 0-game.getGameCamera().getxOffset()),(int)( 0-game.getGameCamera().getyOffset()), null);
			
		
		
		for(Animation e : animationSprites)
		{
			
			e.render(g);
		}
		
		for(Coin c : game.getColDet().getCoins())
		{
			if(c.getMAPNAME().equals(MAPNAME))
			{
				c.render(g);
			}
		}
		for(Turret e : turrets)
		{
			e.render(g);
		}
		
		for(Npc n : npcs)
		{
			n.render(g);
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public ArrayList<BufferedImage> getImageData() {
		return imageData;
	}
	
	
	
	

}
