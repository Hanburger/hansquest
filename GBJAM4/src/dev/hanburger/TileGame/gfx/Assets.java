package dev.hanburger.TileGame.gfx;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Assets {
	
	private static final int width = 8, height=8;
	private static SpriteSheet sheet;
	
	public static BufferedImage player;
	public static BufferedImage textBox;
	
	public static ArrayList<BufferedImage> sprites;
	public static ArrayList<BufferedImage> mapSprites;
	
	public static ArrayList<BufferedImage> walkingUp;
	public static ArrayList<BufferedImage> walkingDown;
	public static ArrayList<BufferedImage> walkingLeft;
	public static ArrayList<BufferedImage> walkingRight;
	
	public static ArrayList<BufferedImage> water;
	
	public static ArrayList<BufferedImage> topPipe;
	public static ArrayList<BufferedImage> middlePipe;
	public static ArrayList<BufferedImage> bottomPipe;
	
	
	public static ArrayList<Integer> animationIDS;
	private static ArrayList<BufferedImage> grass;
	private static ArrayList<BufferedImage> mill;

	public static BufferedImage bigCloud;
	public static BufferedImage smallCloud;
	public static BufferedImage background;
	
	public static ArrayList<BufferedImage> grassW;
	
	public static BufferedImage healthbar;
	public static BufferedImage NPC;
	public static ArrayList<BufferedImage> NPCwalkingLeft;
	public static ArrayList<BufferedImage> NPCwalkingRight;
	public static ArrayList<BufferedImage> waterAN;
	
	
	
	
	

	
	public static void init()
	{
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/sprite_sheet8x8GB7.png"));
		background = ImageLoader.loadImage("/textures/background2.png");
		sprites = new ArrayList<>();
		mapSprites = new ArrayList<>();
		
		player = sheet.crop(2*width, 3*height, width, height);
		
		bigCloud = sheet.crop(16*width,5*height,6*width,2*height);
		smallCloud = sheet.crop(17*width,7*height,4*width,2*height);
		
	
		
		//textBox = sheet.crop(0, height*9,4*width , 2 * height);
		//System.out.println(player.getType());
		
		initializeSprites();
		initializeMapSprites();
		
		healthbar = sprites.get(256);
		
		initializePlayer();
		
		initializeAnimationIdentifiers();
		initializeWaterPipeAnimation();
		initializeGrassAnimation();
		initializeMillAnimation();
		initializeNPC();
		
		initializeWater();
		
		
	}
	
	private static void initializeWater()
	{
		waterAN = new ArrayList<>();
		waterAN.add(sprites.get(229));
		waterAN.add(sprites.get(230));
	}
	
	private static void initializeNPC()
	{
		NPC= sprites.get(97);
		NPCwalkingLeft = new ArrayList<>();
		NPCwalkingLeft.add(sprites.get(161));
		NPCwalkingLeft.add(sprites.get(193));
		
		NPCwalkingRight = new ArrayList<>();
		NPCwalkingRight.add(sprites.get(97));
		NPCwalkingRight.add(sprites.get(129));
		
	}
	
	
	
	private static void initializeWaterPipeAnimation() {
		// TODO Auto-generated method stub
		topPipe = new ArrayList<>();
		topPipe.add(sprites.get(41));
		topPipe.add(sprites.get(41+1));
		topPipe.add(sprites.get(41+2));
		topPipe.add(sprites.get(41+3));
		topPipe.add(sprites.get(41+4));
		//topPipe.add(sprites.get(41+5));
		
		middlePipe = new ArrayList<>();
		middlePipe.add(sprites.get(72));
		middlePipe.add(sprites.get(72+1));
		middlePipe.add(sprites.get(72+2));
		middlePipe.add(sprites.get(72+3));
		middlePipe.add(sprites.get(72+4));
		//middlePipe.add(sprites.get(72+5));
		
		bottomPipe = new ArrayList<>();
		bottomPipe.add(sprites.get(104));
		bottomPipe.add(sprites.get(104+1));
		bottomPipe.add(sprites.get(104+2));
		bottomPipe.add(sprites.get(104+3));
		bottomPipe.add(sprites.get(104+4));
		//bottomPipe.add(sprites.get(104+5));
		
		
	}



	private static void initializeAnimationIdentifiers()
	{
		animationIDS=new ArrayList<>();
		animationIDS.add(33);
		animationIDS.add(34);
		
		animationIDS.add(41);
		animationIDS.add(73);
		animationIDS.add(105);
		
		animationIDS.add(200);
		
		animationIDS.add(230);
		
		
	}
	
	private static void initializeMillAnimation()
	{
		mill = new ArrayList<>();
		
		BufferedImage one= sheet.crop(7*width,6*height, 5*width,5*height);
		BufferedImage two= sheet.crop(7*width,11*height, 5*width,5*height);
		mill.add(one);
		mill.add(two);
		
	}
	
	private static void initializePlayer()
	{
		
		
		walkingLeft = new ArrayList<>();
		walkingLeft.add(sprites.get(99));
		walkingLeft.add(sprites.get(99 +32));
		walkingLeft.add(sprites.get(99));
		walkingLeft.add(sprites.get(99 +64));
		
		
		walkingRight = new ArrayList<>();
		walkingRight.add(sprites.get(98));
		walkingRight.add(sprites.get(98 +32));
		walkingRight.add(sprites.get(98));
		walkingRight.add(sprites.get(98 +64));
		
		
		
	}
	
	private static void initializeWaterAnimation()
	{
		water = new ArrayList<>();
		
		//water.add(sprites.get(160));
	}
	
	private static void initializeGrassAnimation()
	{
		grass = new ArrayList<>();
		grass.add(sprites.get(32));
		grass.add(sprites.get(32+32));
		grassW = new ArrayList<>();
		grassW.add(sprites.get(33));
		grassW.add(sprites.get(33+32));
	}
	
	private static void initializeSprites()
	{
		
		for(int y=0; y < 32; y++)
		{
			for(int x=0; x < 32; x++ )
			{
				sprites.add(sheet.crop(x*width, y *height, width, height));
			}
		}
		
	}
	
	private static void initializeMapSprites()
	{
		mapSprites.add(null);
		
		for(BufferedImage e : sprites)
		{
			mapSprites.add(e);
		}
	}
	
	public static ArrayList<BufferedImage> returnAnimationSprites(int value)
	{
		switch(value)
		{
		case 33:
			return grass;
		case 34:
			return grassW;
		case 41:
			return topPipe;
		case 73:
			return middlePipe;
		case 105:
			return bottomPipe;
			
		case 200:
			return mill;
			
		case 230:
			return waterAN;
			
		}
		
		return null;
		
	
	}
	

}
