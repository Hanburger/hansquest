package dev.hanburger.TileGame.gfx;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.Entity;

public class GameCamera {
	
	private float xOffset, yOffset;
	private Game game;
	
	
	public GameCamera(Game game,float xOffset, float yOffset)
	{
		this.xOffset=xOffset;
		this.yOffset=yOffset;
		this.game=game;
	}
	
	public void checkBlankSpace()
	{
		if(xOffset < 0)
		{
			xOffset=0;
		}
		else if(xOffset > game.getMapManager().returnCurrentMap().getWidth() * 16 - game.width)
		{
			xOffset= game.getMapManager().returnCurrentMap().getWidth() * 16 - game.width;
		}
		
		if(yOffset < 0)
		{
			yOffset=0;
		}
		else if(yOffset > game.getMapManager().returnCurrentMap().getHeight() * 16 - game.height)
		{
			yOffset = game.getMapManager().returnCurrentMap().getHeight() * 16 - game.height;
		}
	}
	
	public void centerOnEntity(Entity e)
	{
		xOffset= e.getX() - game.width /2 + 32/2;
		yOffset = e.getY() - game.height /2 + 32/2;
		checkBlankSpace();
	}

	
	public void move(float xAmt, float yAmt)
	{
		xOffset+=xAmt;
		yOffset+=yAmt;
	}

	public float getxOffset() {
		return xOffset;
	}


	public void setxOffset(float xOffset) {
		this.xOffset = xOffset;
	}


	public float getyOffset() {
		return yOffset;
	}


	public void setyOffset(float yOffset) {
		this.yOffset = yOffset;
	}

}
