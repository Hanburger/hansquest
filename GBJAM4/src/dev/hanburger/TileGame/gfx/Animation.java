package dev.hanburger.TileGame.gfx;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;

public class Animation {
    
    private ArrayList<BufferedImage> animationSprites;
    private int currentSprite = 0; // should be from 0 to sprites.length - 1;
    private float x,y;
	private int offset;
	private float offsetX;
	private float offsetY;
	private Game game;
	private int sizeX,sizeY;
    
    public Animation(ArrayList<BufferedImage> animationSprites,float x, float y, Game game) {
       
       //Fill your array of sprites with sprites of some sort. All should be different
       //if you want to get animation look.
       this.animationSprites = animationSprites;
       this.x=x;
       this.y=y;
       this.game=game;
       offset=0;
       sizeX=16;
       sizeY=16;
       }
    
    
    public void update() {

       currentSprite += 1; // increase frame counter by 1
       
       //check and make sure animation counter is within your sprite
       //array bounds.
       if(currentSprite >= animationSprites.size()) {
          currentSprite = 0; // reset animation to start
       }
       
       
    }
    
    public void setsize(int sizeX,int sizeY)
    {
    	this.sizeX=sizeX;
    	this.sizeY=sizeY;
    }
    
    
    
    public void render(Graphics g) {
    	g.drawImage(animationSprites.get(currentSprite),
    			(int)x-offset-(int)game.getGameCamera().getxOffset() ,
    			(int)y-(int)game.getGameCamera().getyOffset(),sizeX,sizeY, null);
    	
    }
    
    public void updateLoc(float x, float y)
    {
    	this.x=x;
    	this.y=y;
    }
    
    public void setOffset(int offset)
    {
    	this.offset= offset;
    }
    
    public float getX()
    {
    	return x;
    }
    public float getY()
    {
    	return y;
    }
    
    public void setCameraOffset(float x, float y)
    {
    	this.offsetX=x;
    	this.offsetY=y;
    }
}