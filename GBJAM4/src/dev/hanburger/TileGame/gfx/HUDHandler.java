package dev.hanburger.TileGame.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Timer;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.audio.SoundEffect;
import dev.hanburger.TileGame.entities.creatures.Player;
import dev.hanburger.TileGame.misc.Coin;
import dev.hanburger.TileGame.states.EndState;
import dev.hanburger.TileGame.states.State;

public class HUDHandler implements ActionListener{
	
	private Game game;
	private Player player;
	
	private ArrayList<Coin> collectedCoins;
	
	private int max;
	
	private Timer timer;
	
	private int time = 220;
	
	private int timePast=0;
	
	private SoundEffect coinS;
	
	public HUDHandler(Game game,Player player)
	{
		this.game=game;
		this.player=player;
		coinS = new SoundEffect("coin");
		collectedCoins= new ArrayList<>();
//		for(int i = 0 ; i <45 ; i++)
//		{
//			collectedCoins.add(new Coin(0, 0, game, null));
//		}
		max = game.getColDet().getCoins().size();
		timer = new Timer(1000,this);
		//timer.start();
	}
	
	
	public void tick()
	{
		if(!timer.isRunning())
		{
			timer.start();
		}
		
		Coin coin = game.getColDet().checkForCoin(player.getX(), player.getY(),game.getMapManager().returnCurrentMapName());
		
		if(coin!=null && game.getMapManager().returnCurrentMapName() == coin.getMAPNAME())
		{
			collectedCoins.add(coin);
			coinS.play();
		}
//		for(Coin c : game.getColDet().getCoins())
//		{
//			c.setX(c.getX()-game.getGameCamera().getxOffset());
//			c.setY(c.getY()-game.getGameCamera().getyOffset());
//		}
		
		if(time== timePast || collectedCoins.size() == max || player.getHealth()==0)
		{
			State end = new EndState(game,collectedCoins.size(),max,timePast);
			State.setState(end);
		}
		
//		if(collectedCoins.size() == max)
//		{
//			State end = new EndState(game,collectedCoins.size(),max,timePast);
//			State.setState(end);
//		}
	}
	
	public void render(Graphics g)
	{
//		Graphics2D g2 = (Graphics2D) g;
//		g2.setColor(new Color(24,48,48));
//		g2.fillRect(0, 144-19, 100, 19);
//		
//		String line = String.valueOf(collectedCoins.size()) + " / " + String.valueOf(max);
//		g2.setFont(FontLoader.customFont.deriveFont(12f));
//		g2.setColor(new Color(82,127,57));
//		g2.drawImage(Assets.sprites.get(166),5,144-16-3,16,16,null);
//		g2.drawString(line, 30, 144-4);
		
		Graphics2D g2 = (Graphics2D) g;
		//g2.setColor(new Color(24,48,48));
		g2.setColor(new Color(32,70,49));
		g2.fillRect(0, 144-25, 160, 25);
		
		
		g2.setFont(FontLoader.customFont.deriveFont(8f));
		//g2.setColor(new Color(82,127,57));
		g2.setColor(new Color(215,232,148));
		
		//8x8
		//g2.drawString("H: ", 8, 144-15);
		
		//16x16
		g2.drawString("H: ", 8, 144-14);
		
		//8x8
//		for(int i = 0 ; i < player.getHealth(); i++)
//		{
//			g2.drawImage(Assets.healthbar,i*4 + 22,144-24,null);
//		}
		
		//16x16
		for(int i = 0 ; i < player.getHealth(); i++)
		{
			g2.drawImage(Assets.healthbar,i*8 + 19,144-27,16,16,null);
		}
	
		
		String line = String.valueOf(collectedCoins.size()) + " / " + String.valueOf(max);
		
		
		//8x8
//		g2.drawImage(Assets.sprites.get(166),5,144-12-3,12,12,null);
//		g2.drawString(line, 25, 144-3);
		
		//16x16
		g2.drawImage(Assets.sprites.get(166),5,144-12-1,12,12,null);
		g2.drawString(line, 25, 144-1);
		
		g2.drawImage(Assets.sprites.get(168),90,144-25,12,12,null);
		
		g2.drawString(String.valueOf(time - timePast), 109, 144-14);
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		timePast++;
		
		
	}

}
