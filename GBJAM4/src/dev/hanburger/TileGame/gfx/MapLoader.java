package dev.hanburger.TileGame.gfx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;

public class MapLoader {
	
	private BufferedReader in;
	private ArrayList<String> lines;
	private ArrayList<Integer> sprites;
	

	
	private Game game;
	
	
	public MapLoader(Game game) {
		// TODO Auto-generated constructor stub
		this.game=game;
	}

	public Map loadMap(String path,String MapName) throws IOException
	{
		lines = new ArrayList<>();
		sprites = new ArrayList<>();
		in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(path)));
		        
		
		
		String line;
		while((line = in.readLine()) != null)
		{
			lines.add(line);
			
		}
		
		
		in.close();
		
		String width = lines.get(1);
		String height = lines.get(2);
		
		width = width.substring(6);
		
		height = height.substring(7);
		
		int dataLine = 0;
		for(int i = 0 ; i < Integer.valueOf(height); i++)
		{
			String data= lines.get(13+i);
			String[] numbers = data.split(",");
			for(int j = 0 ; j < numbers.length; j++)
			{
				sprites.add(Integer.valueOf(numbers[j]));
			}
			
		}
		
		
		
		
		
		return new Map(game,Integer.parseInt(width), Integer.parseInt(height),sprites,MapName);
	}

}
