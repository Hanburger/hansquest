package dev.hanburger.TileGame.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.entities.Entity;
import dev.hanburger.TileGame.entities.creatures.Npc;
import dev.hanburger.TileGame.entities.creatures.Player;

public class DialogueManager {
	
	
	private Game game;
	private boolean dialogueActivated;
	private Player player;
	private ArrayList<TextBlock> messages;
	private int line =0;;

	public DialogueManager(Game game, Player player)
	{
		this.game=game;
		this.player=player;
		dialogueActivated=false;
	}
	
	
	public void setDialogueActivated(boolean dialogueActivated) {
		this.dialogueActivated = dialogueActivated;
	}


	private void init()
	{
		
	}
	
	public void tick()
	{
		//System.out.println("released:" + game.getKeyManager().xReleased + "," +
				//game.getKeyManager().x);
		//Npc e = game.getColDet().returnCollision(player.getX(),player.getY(), player);
		//if(game.getKeyManager().x)
		//{
			Npc e = returnNpc(player.getLastUsed());
		
			if(e!=null)
			{
				messages = e.getMessages();
				//dialogueActivated=true;
			
			}
			else
			{
				dialogueActivated=false;
				player.unlock();
				
			}
		//}
			
		if(game.getKeyManager().x)
		{
			System.out.println(game.getKeyManager().x  );
			if(e!=null)
			{
				if(line==0 && !dialogueActivated)
				{
					dialogueActivated=!dialogueActivated;
					e.lock();
					player.lock();
				}
				else
				{
					line++;
				}
				
				if(messages.size()<=line)
				{
					line=0;
					dialogueActivated=!dialogueActivated;
					e.unlock();
					player.unlock();
					
				}
			}
		}
		
		
	}
	
	private Npc returnNpc(String lastUsed)
	{
		Npc e=null;
		switch(lastUsed)
		{
		case "w":
			e= game.getColDet().returnNPCCollision(player.getX(),player.getY()-2, player);
			break;
		case "d":
			e= game.getColDet().returnNPCCollision(player.getX(),player.getY()+2, player);
			break;
		case "l":
			e= game.getColDet().returnNPCCollision(player.getX()-2,player.getY(), player);
			break;
		case "r":
			e = game.getColDet().returnNPCCollision(player.getX()+2,player.getY(), player);
			break;
			
			 
		}
		
		return e;
	}
	
	public void render(Graphics g)
	{
		
		if(dialogueActivated)
		{
			g.drawImage(Assets.textBox, 0, 360, 480,120,null);
			//g.drawImage(Assets.textBox,(int) player.getX()+40-(int)game.getGameCamera().getxOffset(),
					//(int)player.getY()-80 -(int)game.getGameCamera().getyOffset(), 120, 80, null);
			g.setColor(Color.BLACK);
			g.setFont(FontLoader.customFont.deriveFont(24f));
			
			g.drawString(messages.get(line).getLineOne(), 40, 410);
			if(messages.get(line).getLineTwo()!=null)
			{
				g.drawString(messages.get(line).getLineTwo(), 40, 450);
			}
		}
	}

}
