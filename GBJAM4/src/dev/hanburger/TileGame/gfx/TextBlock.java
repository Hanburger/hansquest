package dev.hanburger.TileGame.gfx;

public class TextBlock {
	
	
	private String lineOne;
	private String lineTwo;
	
	public TextBlock(String one, String two)
	{
		lineOne=one;
		lineTwo=two;
	}

	public String getLineOne() {
		return lineOne;
	}

	public String getLineTwo() {
		return lineTwo;
	}

}
