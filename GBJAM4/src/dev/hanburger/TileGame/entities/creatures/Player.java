package dev.hanburger.TileGame.entities.creatures;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.audio.SoundEffect;
import dev.hanburger.TileGame.gfx.Animation;
import dev.hanburger.TileGame.gfx.Assets;

public class Player extends Creature{
	
	
	
	
	
	private String lastUsed="";
	private boolean cx,cy;
	
	
	private int animationIndex;
	private Animation up,down,left,right;
	private ArrayList<Animation> directions;
	private boolean isWalking;
	private int animationCounter=0;
	private boolean locked=false;
	private boolean isJumping;
	private float curve=3.5f;
	private float yOriginal=0;
	private boolean falling=false;
	private int jumpcounter=0;
	
	private String currentMap="LEVEL01";
	private boolean wReleased=false;
	private int xBorder=16*25;
	private int yBorder=0;
	private SoundEffect jump;
	
	
	
	
	


	public Player(Game game, float x, float y) {
		super(game,x, y);
	
		animationIndex=0;
		isJumping=false;
		initAnimations();
		
		jump = new SoundEffect("Jump3");
		 
		
		 
		
	}
	
	public String getCurrentMap()
	{
		return currentMap;
	}
	
	public void setCurrentMap(String MAP)
	{
		currentMap=MAP;
	}
	
	public void initAnimations()
	{

		left = new Animation(Assets.walkingLeft, x, y,game);
		left.setOffset(2);
		right = new Animation(Assets.walkingRight, x, y,game);
		right.setOffset(-2);
//		
		directions = new ArrayList<>();

		directions.add(left);
		directions.add(right);
		isWalking=false;
	}

	@Override
	public void tick() {
		//System.out.println(health);

		box.setRect(x, y, 16, 16);
		

		
		if(!locked)
		{
		update();
		
		
		move();
		
		
		updateCamera();
		
		
		if(isJumping==false)
		{
			physics();
		}
		
		if(isJumping ==true)
		{
			jump();
		}
		
		//System.out.println(x + ", " + y + ", " + isJumping + ", " + falling + ", " + curve);
		}	
		
		
		
		
		
		}
	
	
	private void physics() {
		// TODO Auto-generated method stub
		if(!game.getColDet().checkCollision(x, y+3, this,currentMap))
		{
			y+=3;
			directions.get(animationIndex).updateLoc(x, y);
			
			
			
		}
		
		if(!game.getColDet().checkCollision(x, y+1, this,currentMap))
		{
			y+=1;
			directions.get(animationIndex).updateLoc(x, y);
		}
		
//		if(!game.getColDet().checkCollision(x, y+1, this, currentMap)&& game.getColDet().checkCollision(x, y+2, this, currentMap))
//		{
//			y+=1;
//			directions.get(animationIndex).updateLoc(x, y);
//		}
		
		if(game.getColDet().checkCollision(x, y, this, currentMap))
		{
			y-=2;
		}
		
		
		
	}
	
	private void jump()
	{
		isJumping=true;
		if(!falling)
		{
			y=y-curve;
		}
		else
		{
			y=y+curve;
		}
		directions.get(animationIndex).updateLoc(x,y);
		if(jumpcounter<1)
		{
			jumpcounter++;
		}
		else
		{
			jumpcounter=0;
			if(!falling)
			{
				curve-=0.5;
			}
			else
			{
				curve+=0.5;
			}
			
			if(curve==0)
			{
				falling=true;
			}
			
		}
//		if(curve==4 && falling)
//		{
//			
//			isJumping=false;
//			falling=false;
//			curve=4;
//		}
//		
//		if(falling && game.getColDet().checkCollision(x, y+curve, this,currentMap))
//		{
//			isJumping=false;
//			falling=false;
//			curve=4;
//		}
//		
//		if(falling && game.getColDet().checkCollision(x, y+1, this,currentMap))
//		{
//			//y+=1;
//			isJumping=false;
//			falling=false;
//			curve=4;
//		}
		
		if(falling && y == yOriginal )
		{
			isJumping=false;
			falling=false;
			curve=4;
		}
		//System.out.println(game.getColDet().checkCollision(x, y, this, currentMap) + "," +
				//game.getColDet().checkCollision(x, y, this, currentMap));
		if(falling &&!game.getColDet().checkCollision(x, y, this, currentMap) && game.getColDet().checkCollision(x, y+3, this, currentMap))
		{
			isJumping=false;
			falling=false;
			curve=4;
		}
//		
		
		
	}
	
	public void setBorders(int borderx,int bordery)
	{
		xBorder=borderx;
		yBorder=bordery;
	}

	public void lock()
	{
		locked=true;
	}
	
	public void unlock()
	{
		locked = false;
	}
	
	public String getLastUsed() {
		return lastUsed;
	}

	private void updateCamera()
	{
		for(Animation e: directions)
		{
			e.setCameraOffset(game.getGameCamera().getxOffset(), game.getGameCamera().getyOffset());
		}
		
		game.getGameCamera().centerOnEntity(this);
		
		
	}
	
	private void update()
	{
		for(int i = 0 ; i <2 ; i++)
		{
			directions.get(i).updateLoc(x, y);
		}
		
		
		if(isWalking==true)
		{
			if(animationCounter >7)
			{
				animationCounter=0;
				directions.get(animationIndex).update();
			}
			else
			{
				animationCounter++;
			}
		}
		else
		{
			animationCounter=0;
		}
	}
	
	private void move()
	{
		
		if(game.getKeyManager().up)
		{
//			animationIndex=0;
//			isWalking=true;
//			lastUsed = "w";
//			if(!game.getColDet().checkCollision(x, y-2,this))
//			{
//				
//				animationIndex=0;
//				y -=2;
//				//lastUsed = "w";
//				
//			}
			//System.out.println("pressed");
			
			if(isJumping==false &&wReleased)
			{
				yOriginal=y;
				
			}
			
			
			if(isJumping==false && game.getColDet().checkCollision(x, y+2, this,currentMap) && wReleased)
			{
				//System.out.println("called");
				jump.play();
				jump();
			}
			wReleased=false;
		}
		else
		{
			wReleased=true;
		}
		if(game.getKeyManager().down )
		{
//			animationIndex=1;
//			isWalking=true;
//			lastUsed = "d";
//			if(!game.getColDet().checkCollision(x, y+1,this))
//			{
//				y += 2;
//				animationIndex=1;
//				//lastUsed = "d";
//			}
		}
		if(game.getKeyManager().left )
		{
			animationIndex =0;
			isWalking=true;
			lastUsed = "l";
			if(!game.getColDet().checkCollision(x-2, y,this,currentMap) || isJumping)
			{
				if(x-2 >0)
				x -= 2;
				animationIndex =0;
				//lastUsed = "l";
			}
		}
		else if(game.getKeyManager().right)
		{
			animationIndex= 1;
			isWalking=true;
			lastUsed = "r";
			if(!game.getColDet().checkCollision(x+2, y,this,currentMap) || isJumping)
			{
				if(x+2 <xBorder-16)
				x += 2;
				animationIndex= 1;
				//lastUsed = "r";
			}
		}
		else
		{
			isWalking=false;
		}
		
	
		
		
		
		
		
		
	}
		

	@Override
	public void render(Graphics g) {
		String test = "playerA";
		;
		
		if(isWalking==true)
		{
			directions.get(animationIndex).render(g);
		}
		else{
			g.drawImage(getIdleImage(animationIndex),
					(int) x - (int) game.getGameCamera().getxOffset(),
					(int) y - (int) game.getGameCamera().getyOffset(),16,16,null);
		}
		
		
	}
	
	private BufferedImage getIdleImage(int index)
	{
		switch(index)
		{
	
		case 0: return Assets.walkingLeft.get(0);
		case 1: return Assets.walkingRight.get(0);
		
		}
		return null;
	}
	
	
	private boolean isInTile(float coord)
	{
		if(coord%16 ==0)
		{
			return true;
		}
		return false;
		
	}
	
	public float getXCoords()
	{
		return x;
	}
	public float getYCoords()
	{
		return y;
	}
	
	
	
	
	public void setCoords(float x, float y)
	{
		this.x=x;
		this.y=y;
	}
	

}
