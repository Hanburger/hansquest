package dev.hanburger.TileGame.entities.creatures;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Animation;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.gfx.TextBlock;


public class Npc extends Creature{
	
	private Game game;
	private float x,y;
	private String lastUsed= "";

	private boolean cx,cy;
	
	private int chance = 0;
	private int animationIndex;
	
	private boolean locked=false;
	
	private Random random;
	
	private ArrayList<TextBlock> messages;
	private String currentMap;
	private Animation left;
	private Animation right;
	private int animationCounter=0;
	
	private int orx,ory;
	
	public Npc(Game game, float x, float y,String currentMap)
	{
		super(game,x,y);
		this.x=x;
		this.y=y;
		this.orx=(int) x;
		this.ory=(int) y;
		this.game = game;
		random = new Random();
		this.messages= new ArrayList<>();
		messages.add(new TextBlock("I am the villager","of this town!"));
		messages.add(new TextBlock("My name is Han!",null));
		this.currentMap=currentMap;
		left = new Animation(Assets.NPCwalkingLeft, x, y, game);
		right = new Animation(Assets.NPCwalkingRight, x, y, game);
		
		//messages.add("My name is Han!");
		
		this.animationIndex=0;
		
	}

	public ArrayList<TextBlock> getMessages() {
		return messages;
	}
	
	private void physics() {
		// TODO Auto-generated method stub
		if(!game.getColDet().checkCollision(x, y+3, this,currentMap))
		{
			y+=3;
			
			left.updateLoc(x, y);
			right.updateLoc(x, y);
			
			
		}
		
		if(!game.getColDet().checkCollision(x, y+1, this,currentMap))
		{
			y+=1;
			left.updateLoc(x, y);
			right.updateLoc(x, y);
		}
		
//		if(!game.getColDet().checkCollision(x, y+1, this, currentMap)&& game.getColDet().checkCollision(x, y+2, this, currentMap))
//		{
//			y+=1;
//			directions.get(animationIndex).updateLoc(x, y);
//		}
		
		if(game.getColDet().checkCollision(x, y, this, currentMap))
		{
			y-=2;
		}
	}
		

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		if(y > 500)
		{
			y= ory;
			x=orx;
		}
		//CollisionDetection.rectangles.set(colID, new Rectangle2D.Float(x,y,32,32));
		physics();
		
		box.setRect(x, y, 40, 40);
		
		left.updateLoc(x, y);
		right.updateLoc(x, y);
		
		if(animationCounter > 20)
		{	
		left.update();
		right.update();
		animationCounter=0;
		}
		else
		{
			animationCounter++;
		}
		
		cx = isInTile(x);
		cy = isInTile(y);
		
		chance = random.nextInt(500);
		//System.out.println(chance);
		
		if(cy && cx)
		{
			if(!locked)
			{
			
			
			if(chance == 7)
			{
				animationIndex=3;
				//System.out.println("left");
				if(!game.getColDet().checkCollision(x-2, y,this,currentMap))
				{
					animationIndex=3;
					x-=2;
					lastUsed="l";
				}
			}
			if(chance ==9)
			{
				animationIndex=1;
				//System.out.println("right + " + chance);
				if(!game.getColDet().checkCollision(x+2, y,this,currentMap))
				{
					animationIndex=1;
					//System.out.println("Initiate");
					x+=2;
					lastUsed="r";
				}
			}
			
			}
		}
		
//		if(!cy)
//		{
//			
//			if(lastUsed.equals("w"))
//			{
//				if(!game.getColDet().checkCollision(x, y-2,this,currentMap))
//				y-=2;
//				else
//					y+=2;
//			}
//			if(lastUsed.equals("d"))
//				if(!game.getColDet().checkCollision(x, y+2,this,currentMap))
//					y+=2;
//					else
//						y-=2;
//		}
		else{
		}
		if(!cx)
		{
			if(lastUsed.equals("l"))
				if(!game.getColDet().checkCollision(x-1, y,this,currentMap))
				x-=1;
				else
					x+=1;
			if(lastUsed.equals("r"))
				if(!game.getColDet().checkCollision(x+1, y,this,currentMap))
				x+=1;
				else
					x-=1;
		}
		else{
		}
		
		
	}
	
	public void lock()
	{
		locked=true;
	}
	
	public void unlock()
	{
		locked=false;
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		//g.drawImage(Assets.sprites.get(0), (int) x - (int)game.getGameCamera().getxOffset(),
				//(int) y - (int)game.getGameCamera().getyOffset(), 40,40, null);
		
		if(cx)
		{
			if(lastUsed=="l")
			{
				g.drawImage(Assets.NPCwalkingLeft.get(0),(int)x-(int) game.getGameCamera().getxOffset(),
						(int)y-(int)game.getGameCamera().getyOffset(),16,16,null);
			}
			else
			{
				g.drawImage(Assets.NPCwalkingRight.get(0),(int)x-(int) game.getGameCamera().getxOffset(),
						(int)y-(int)game.getGameCamera().getyOffset(),16,16,null);
			}
			
//			g.drawImage(Assets.NPC,(int)x-(int) game.getGameCamera().getxOffset(),
//					(int)y-(int)game.getGameCamera().getyOffset(),16,16,null);
		}
		else
		{
			if(lastUsed=="l")
			{
				left.render(g);
			}
			else
			{
				right.render(g);
			}
		}
		
	}
	
	
	private boolean isInTile(float coord)
	{
		if(coord%16 ==0)
		{
			return true;
		}
		return false;
		
	}
	public void setCoords(float x, float y)
	{
		this.x=x;
		this.y=y;
	}
	
//	private BufferedImage returnImage(String direction)
//	{
//		switch(direction)
//		{
//		case "w": return Assets.sprites.get(193);
//		
//		case "d": return Assets.sprites.get(0);
//		
//		case "l": return Assets.sprites.get(192);
//		
//		case "r": return Assets.sprites.get(194);
//		
//		}
//		return null;
//	
//	}

}
