package dev.hanburger.TileGame.entities.creatures;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;

import dev.hanburger.TileGame.Game;
import dev.hanburger.TileGame.gfx.Assets;
import dev.hanburger.TileGame.misc.Projectile;

public class Turret{

	private Game game;
	private float x;
	private float y;
	private Rectangle2D.Float box;
	private ArrayList<Projectile> missiles;
	private int speed;
	private int counter=0;
	private int mapWidth;

	public Turret(Game game, float x, float y,int speed, int mapWidth)
	{
		this.game=game;
		this.x=x;
		this.y=y;
		box = new Rectangle2D.Float(x,y,16,16);
		missiles = new ArrayList<>();
		this.speed=speed;
		this.mapWidth=mapWidth;
		
		
	
		
		
		
	}
	
	public Rectangle2D.Float getBox() {
		return box;
	}
	
	public ArrayList<Projectile> getMissiles()
	{
		return missiles;
	}

	public void tick()
	{
		if(counter> speed)
		{
			missiles.add(new Projectile(x+16,y+16,"RIGHT",game));
			counter=0;
			System.out.println("shot");
		}
		else
		{
			counter++;
		}
		
		for(Projectile e : missiles)
		{
			e.tick();
		}
		
		Iterator<Projectile> it = missiles.iterator();
		
		while(it.hasNext())
		{
			Projectile mi = it.next();
			
			if(mi.getX() > mapWidth *16)
			{
				it.remove();
				
			}
		}
		
		
	}
	
	public void render(Graphics g)
	{
		g.drawImage(Assets.sprites.get(172),(int)x-(int)game.getGameCamera().getxOffset(),
				(int)y-(int)game.getGameCamera().getyOffset(),16,16,null);
		for(Projectile e: missiles)
		{
			e.render(g);
			
		}
	}

}
