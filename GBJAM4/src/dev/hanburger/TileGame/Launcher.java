package dev.hanburger.TileGame;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import dev.hanburger.TileGame.gfx.FontLoader;



public class Launcher {

	public static void main(String[] args)
	{
		FontLoader.loadFont();
		
		
		//int answer = JOptionPane.showConfirmDialog(null, "Do you want to play with the Original Gameboy Resolution?(160x144)");
		
		
//		if(answer == JOptionPane.YES_OPTION)
//		{
//			Game game = new Game("GBJAM4",160,144,16);
//			game.start();
//		}
//		if(answer == JOptionPane.NO_OPTION)
//		{
//			Game game = new Game("GBJAM4",320,288,32);
//			game.start();
//		}
		
		Game game = new Game("GBJAM4",160,144,16);
		game.start();
		
	}
	
}
